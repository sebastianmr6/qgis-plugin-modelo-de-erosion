# Instalación
Para compilar el plugin dentro de su QGIS usted debe ejecutar el comando
pb_tool deploy {UNIDAD}:/Users/{USUARIO}/AppData/Roaming/QGIS/QGIS3/profiles/{PERFIL DE QGIS}/python/plugins

En el archivo Makefile en la línea 68
QGISDIR={UNIDAD}:\Users\{USUARIO}\AppData/Roaming/QGIS/QGIS3/profiles/default/python/plugins

# Descripción
El siguiente es es modelo creado en QGIS 3.10 y que fue convertido en un complemento
![Imagen de modelo en QGIS](documentacion/images/qgis_model_designer.svg)