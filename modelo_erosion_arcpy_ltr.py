# -*- coding: utf-8 -*-

from qgis.core import QgsProcessing
from qgis.core import QgsProcessingAlgorithm
from qgis.core import QgsProcessingMultiStepFeedback
from qgis.core import QgsProcessingParameterVectorLayer
from qgis.core import QgsProcessingParameterField
from qgis.core import QgsProcessingParameterRasterLayer
from qgis.core import QgsProcessingParameterRasterDestination
import processing

import os

from qgis.PyQt.QtCore import QSettings, QTranslator, QCoreApplication
from qgis.PyQt.QtGui import QIcon
from qgis.PyQt.QtWidgets import QAction

from qgis.core import QgsProject, QgsMapLayerProxyModel, QgsRasterLayer, QgsFieldProxyModel
from qgis.analysis import QgsRasterCalculator, QgsRasterCalculatorEntry
from qgis.utils import iface

# Initialize Qt resources from file resources.py
from .resources import *
# Import the code for the dialog
from .modelo_erosion_arcpy_ltr_dialog import ModeloErosionArcpyLTRDialog
### llamar model ode arcpy acá

class ModeloErosionArcpyLTR:
    """QGIS Plugin Implementation."""

    def __init__(self, iface):
        """Constructor.

        :param iface: An interface instance that will be passed to this class
            which provides the hook by which you can manipulate the QGIS
            application at run time.
        :type iface: QgsInterface
        """
        # Save reference to the QGIS interface
        self.iface = iface
        # initialize plugin directory
        self.plugin_dir = os.path.dirname(__file__)
        # initialize locale
        locale = QSettings().value('locale/userLocale')[0:2]
        locale_path = os.path.join(
            self.plugin_dir,
            'i18n',
            'ModeloErosionArcpyLTR_{}.qm'.format(locale))

        if os.path.exists(locale_path):
            self.translator = QTranslator()
            self.translator.load(locale_path)
            QCoreApplication.installTranslator(self.translator)

        # Declare instance attributes
        self.actions = []
        self.menu = self.tr(u'&Modelo de erision usando arcpy LTR')

        # Check if plugin was started the first time in current QGIS session
        # Must be set in initGui() to survive plugin reloads
        self.first_start = None

    # noinspection PyMethodMayBeStatic
    def tr(self, message):
        """Get the translation for a string using Qt translation API.

        We implement this ourselves since we do not inherit QObject.

        :param message: String for translation.
        :type message: str, QString

        :returns: Translated version of message.
        :rtype: QString
        """
        # noinspection PyTypeChecker,PyArgumentList,PyCallByClass
        return QCoreApplication.translate('ModeloErosionArcpyLTR', message)


    def add_action(
        self,
        icon_path,
        text,
        callback,
        enabled_flag=True,
        add_to_menu=True,
        add_to_toolbar=True,
        status_tip=None,
        whats_this=None,
        parent=None):
        """Add a toolbar icon to the toolbar.

        :param icon_path: Path to the icon for this action. Can be a resource
            path (e.g. ':/plugins/foo/bar.png') or a normal file system path.
        :type icon_path: str

        :param text: Text that should be shown in menu items for this action.
        :type text: str

        :param callback: Function to be called when the action is triggered.
        :type callback: function

        :param enabled_flag: A flag indicating if the action should be enabled
            by default. Defaults to True.
        :type enabled_flag: bool

        :param add_to_menu: Flag indicating whether the action should also
            be added to the menu. Defaults to True.
        :type add_to_menu: bool

        :param add_to_toolbar: Flag indicating whether the action should also
            be added to the toolbar. Defaults to True.
        :type add_to_toolbar: bool

        :param status_tip: Optional text to show in a popup when mouse pointer
            hovers over the action.
        :type status_tip: str

        :param parent: Parent widget for the new action. Defaults None.
        :type parent: QWidget

        :param whats_this: Optional text to show in the status bar when the
            mouse pointer hovers over the action.

        :returns: The action that was created. Note that the action is also
            added to self.actions list.
        :rtype: QAction
        """

        icon = QIcon(icon_path)
        action = QAction(icon, text, parent)
        action.triggered.connect(callback)
        action.setEnabled(enabled_flag)

        if status_tip is not None:
            action.setStatusTip(status_tip)

        if whats_this is not None:
            action.setWhatsThis(whats_this)

        if add_to_toolbar:
            # Adds plugin icon to Plugins toolbar
            self.iface.addToolBarIcon(action)

        if add_to_menu:
            self.iface.addPluginToMenu(
                self.menu,
                action)

        self.actions.append(action)

        return action

    def initGui(self):
        """Create the menu entries and toolbar icons inside the QGIS GUI."""

        icon_path = ':/plugins/modelo_erosion_arcpy_ltr/icon.png'
        self.add_action(
            icon_path,
            text=self.tr(u'Modelo erosión LTR'),
            callback=self.run,
            parent=self.iface.mainWindow())



        # will be set False in run()

        self.first_start = True

    def unload(self):
        """Removes the plugin menu item and icon from QGIS GUI."""
        for action in self.actions:
            self.iface.removePluginMenu(
                self.tr(u'&Modelo de erision usando arcpy LTR'),
                action)
            self.iface.removeToolBarIcon(action)

    def btnProcess_onClick2(self):
        print(self.dlg.cmbFieldUsosDelSuelo.currentField())

    def run(self):
        """Run method that performs all the real work"""

        # Create the dialog with elements (after translation) and keep reference
        # Only create GUI ONCE in callback, so that it will only load when the plugin is started
        if self.first_start == True:
            self.first_start = False
            self.dlg = ModeloErosionArcpyLTRDialog()


        # show the dialog
        self.dlg.show()

        self.dlg.btn_ejecutar.clicked.connect(self.btnProcess_onClick)

        # Combobox DEM
        self.dlg.cmboxDEM.setShowCrs(True) # Mostrar sistema de coordenadas
        self.dlg.cmboxDEM.setFilters(QgsMapLayerProxyModel.RasterLayer) #  Filtrar combo box
        self.capa_dem = self.dlg.cmboxDEM.currentLayer()
        # Combobox Shape Usos del Suelo
        self.dlg.cmbUsosDelSuelo.setShowCrs(True)
        self.dlg.cmbUsosDelSuelo.setFilters(QgsMapLayerProxyModel.PolygonLayer)
        self.capa_usos_del_suelo = self.dlg.cmbUsosDelSuelo.currentLayer()
        # Combobox Field Usos de suelo
        self.dlg.cmbFieldUsosDelSuelo.setFilters(QgsFieldProxyModel.Double)
        self.dlg.cmbFieldUsosDelSuelo.setLayer(self.capa_usos_del_suelo)
        self.dlg.cmbUsosDelSuelo.layerChanged.connect(self.dlg.cmbFieldUsosDelSuelo.setLayer)
        # Combobox Shape Edafología
        self.dlg.cmbEdafologia.setShowCrs(True)
        self.dlg.cmbEdafologia.setFilters(QgsMapLayerProxyModel.PointLayer)
        self.capa_edafologia = self.dlg.cmbEdafologia.currentLayer()
        # Combobox Field Edafología
        self.dlg.cmbFieldEdafologia.setFilters(QgsFieldProxyModel.Double)
        self.dlg.cmbFieldEdafologia.setLayer(self.capa_edafologia)
        self.dlg.cmbEdafologia.layerChanged.connect(self.dlg.cmbFieldEdafologia.setLayer)
        # Combobox Shape Estaciones de Monitoreo
        self.dlg.cmbEstacionesDeMonitoreo.setShowCrs(True)
        self.dlg.cmbEstacionesDeMonitoreo.setFilters(QgsMapLayerProxyModel.PointLayer)
        self.capa_estaciones_monitoreo = self.dlg.cmbEstacionesDeMonitoreo.currentLayer()
        # Combobox field Estaciones de monitoreo
        self.dlg.cmbLayerEstacionesMonitoreo.setFilters(QgsFieldProxyModel.Double)
        self.dlg.cmbLayerEstacionesMonitoreo.setLayer(self.capa_estaciones_monitoreo)
        self.dlg.cmbEstacionesDeMonitoreo.layerChanged.connect(self.dlg.cmbLayerEstacionesMonitoreo.setLayer)

        self.dlg.txtPathOut.setDialogTitle('Carpeta de salida de los archivos')
        self.dlg.txtPathOut.setStorageMode(1)

        if self.capa_dem is not None and self.capa_usos_del_suelo is not None and self.capa_edafologia is not None and self.capa_estaciones_monitoreo is not None:
            self.dlg.btn_ejecutar.setEnabled(True)
        else:
            self.dlg.btn_ejecutar.setEnabled(False)


    def btnProcess_onClick(self):
        self.dlg.btn_ejecutar.setEnabled(False)

        parametros = {
            'DEM' :         self.capa_dem.source(),
            'edafologia' :  self.capa_edafologia.source(),
            'estaciones' :  self.capa_estaciones_monitoreo.source(),
            'landuses' :    self.capa_usos_del_suelo.source(),

            # Outputs
            'factor_C' :        os.path.join(self.dlg.txtPathOut.filePath(), 'factor_C.tif'),
            'terraflow_fill' :  os.path.join(self.dlg.txtPathOut.filePath(), 'grass_terraflow_fill.tif'),
            'terraflow_fill_accumulation' : os.path.join(self.dlg.txtPathOut.filePath(), 'qgis_grass_accumulation.tif'),
            'terraflow_fill_direction' :    os.path.join(self.dlg.txtPathOut.filePath(), 'grass_terraflow_direction.tif'),
            'factor_K' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_K.tif'),
            'factor_F' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_F.tif'),
            'factor_M' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_M.sdat'),
            'factor_L' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_L.tif'),
            'factor_S' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_S.tif'),
            'factor_L_S' :  os.path.join(self.dlg.txtPathOut.filePath(), 'factor_L_S.tif'),
            'factor_A' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_A.tif'),
            'qgis_slope' :  os.path.join(self.dlg.txtPathOut.filePath(), 'slope.tif'),
            'factor_R' :    os.path.join(self.dlg.txtPathOut.filePath(), 'factor_R.sdat')
            }

        if self.dlg.txtPathOut.filePath() is not '':

            self.dlg.progressBar.setValue(0)
            self.outputs, self.results =  self.ejecutarRasterize(parametros)
            self.dlg.progressBar.setValue(10)
            self.outputs, self.results =  self.ejecutarInterpolacionBSpline(parametros,
                                                                            self.outputs, self.results)
            self.dlg.progressBar.setValue(20)
            self.outputs, self.results = self.ejecutarTerraflow(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(30)
            self.outputs, self.results = self.ejecutarSlope(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(40)
            self.outputs, self.results = self.ejecutarFactorF(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(50)
            self.outputs, self.results = self.ejecutarFactorM(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(60)
            self.outputs, self.results = self.ejecutarFactorL(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(70)
            self.outputs, self.results = self.ejecutarFactorS(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(80)
            self.outputs, self.results = self.ejecutarFactorR(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(90)
            self.outputs, self.results = self.ejecutarFactorK(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(95)
            self.outputs, self.results = self.ejecutarFactorC(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(98)
            self.outputs, self.results = self.ejecutarFactorLS(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(99)
            self.outputs, self.results = self.ejecutarFactorA(parametros, self.outputs, self.results)
            self.dlg.progressBar.setValue(100)
        else:
            self.dlg.btn_ejecutar.setEnabled(True)

    def ejecutarRasterize(self, parameters):  # Ok
        results = {}
        outputs = {}

        # Rasterize (vector to raster)
        alg_params = {
            'BURN': 0,
            'DATA_TYPE': 5,
            'EXTENT': parameters['DEM'],
            'EXTRA': '',
            'FIELD': 'Factor_C',
            'HEIGHT': 39,
            'INIT': None,
            'INPUT': parameters['landuses'],
            'INVERT': False,
            'NODATA': 0,
            'OPTIONS': '',
            'UNITS': 0,
            'WIDTH': 39,
            'OUTPUT': parameters['factor_C']
        }
        outputs['RasterizeVectorToRaster'] = processing.run('gdal:rasterize', alg_params)
        results['Factor_c'] = outputs['RasterizeVectorToRaster']['OUTPUT']
        return outputs, results

    def ejecutarInterpolacionBSpline(self, parameters, outputs, results):  # Ok
        # Multilevel b-spline interpolation
        alg_params = {
            'EPSILON': 0.0001,
            'FIELD': 'Precipitac',
            'LEVEL_MAX': 11,
            'METHOD': 0,
            'SHAPES': parameters['estaciones'],
            'TARGET_USER_FITS': 0,
            'TARGET_USER_SIZE': 35,
            'TARGET_USER_XMIN TARGET_USER_XMAX TARGET_USER_YMIN TARGET_USER_YMAX': parameters['DEM'],
            'TARGET_OUT_GRID': parameters['factor_R']
        }
        outputs['MultilevelBsplineInterpolation'] = processing.run('saga:multilevelbsplineinterpolation', alg_params)
        results['Factor_r'] = outputs['MultilevelBsplineInterpolation']['TARGET_OUT_GRID']
        return outputs, results

    def ejecutarTerraflow(self, parameters, outputs, results):  # Ok
        # r.terraflow
        alg_params = {
            '-s': False,
            'GRASS_RASTER_FORMAT_META': '',
            'GRASS_RASTER_FORMAT_OPT': '',
            'GRASS_REGION_CELLSIZE_PARAMETER': 0,
            'GRASS_REGION_PARAMETER': None,
            'd8cut': None,
            'elevation': parameters['DEM'],
            'memory': 300,
            'accumulation': parameters['terraflow_fill_accumulation'],
            'direction': parameters['terraflow_fill_direction'],
            'filled': parameters['terraflow_fill'],
            'stats': QgsProcessing.TEMPORARY_OUTPUT,
            'swatershed': QgsProcessing.TEMPORARY_OUTPUT,
            'tci': QgsProcessing.TEMPORARY_OUTPUT
        }
        outputs['Rterraflow'] = processing.run('grass7:r.terraflow', alg_params)
        results['Terraflow_fill'] = outputs['Rterraflow']['filled']
        results['Terraflow_fill_accumulation'] = outputs['Rterraflow']['accumulation']
        results['Terraflow_fill_direction'] = outputs['Rterraflow']['direction']

        return outputs, results

    def ejecutarSlope(self, parameters, outputs, results):      # Ok
        # Slope
        alg_params = {
            'INPUT': outputs['Rterraflow']['filled'],
            'Z_FACTOR': 1,
            'OUTPUT': parameters['qgis_slope']
        }
        outputs['Slope'] = processing.run('qgis:slope', alg_params)
        results['Qgis_slope'] = outputs['Slope']['OUTPUT']

        return outputs, results

    def ejecutarFactorF(self, parameters, outputs, results):    # Ok
        # Raster Calculator 1
        alg_params = {
            'CELLSIZE': 0,
            'CRS': None,
            'EXPRESSION': '((Sin(\"slope@1\"*0.01745) / 0.0896) / (3 * (Sin(\"slope@1\"*0.01745) ^ 0.8)+0.56)) ',
            'EXTENT': outputs['Slope']['OUTPUT'],
            'LAYERS': outputs['Slope']['OUTPUT'],
            'OUTPUT': parameters['factor_F']
        }
        outputs['RasterCalculator1'] = processing.run('qgis:rastercalculator', alg_params)
        results['Factor_f'] = outputs['RasterCalculator1']['OUTPUT']

        return outputs, results

    def ejecutarFactorM(self, parameters, outputs, results):    # Ok
        # Raster calculator 2
        alg_params = {
            'CELLSIZE': 0,
            'CRS': None,
            'EXPRESSION': '\"factor_F@1\" /  ( 1 + \"factor_F@1\" ) ',
            'EXTENT': None,
            'LAYERS': outputs['RasterCalculator1']['OUTPUT'],
            'OUTPUT': parameters['factor_M']
        }
        outputs['RasterCalculator2'] = processing.run('qgis:rastercalculator', alg_params)
        results['Factor_m'] = outputs['RasterCalculator2']['OUTPUT']
        iface.addRasterLayer(outputs['RasterCalculator2']['OUTPUT'])
        return outputs, results

    def ejecutarFactorL(self, parameters, outputs, results):    # Ok
        # Raster calculator 3
        alg_params = {
            'CELLSIZE': 0,
            'CRS': None,
            'EXPRESSION': '( ( (\"qgis_grass_accumulation@1\" + 625 )  ^  ( \"factor_M@1\" +  1 )  )  -  (  \"qgis_grass_accumulation@1\" ^  ( \"factor_M@1\" +  1 )  )  )  /  (   ( 25 ^  ( \"factor_M@1\"  + 2 )   )    *    ( 22.13 ^  ( \"factor_M@1\" )   )       )',
            'EXTENT': outputs['Rterraflow']['accumulation'],
            'LAYERS': [outputs['Rterraflow']['accumulation'],outputs['RasterCalculator2']['OUTPUT']],
            'OUTPUT': parameters['factor_L']
        }
        outputs['RasterCalculator3'] = processing.run('qgis:rastercalculator', alg_params)
        results['Factor_l'] = outputs['RasterCalculator3']['OUTPUT']
        iface.addRasterLayer(outputs['RasterCalculator3']['OUTPUT'])
        return outputs, results

    def ejecutarFactorS(self, parameters, outputs, results):    # Ok
        # Raster calculator 4
        alg_params = {
            'CELLSIZE': 0,
            'CRS': None,
            'EXPRESSION': ' ( (Tan(\"slope@1\" * 0.01745) < 0.09 ) * (10.8 * Sin(\"slope@1\" * 0.01745) + 0.03) ) + ( (Tan(\"slope@1\" * 0.01745) >= 0.09 )  * (16.8 * Sin(\"slope@1\" * 0.01745) - 0.5)  ) ',
            'EXTENT': outputs['Slope']['OUTPUT'],
            'LAYERS': outputs['Slope']['OUTPUT'],
            'OUTPUT': parameters['factor_S']
        }
        outputs['RasterCalculator4'] = processing.run('qgis:rastercalculator', alg_params)
        results['Factor_s'] = outputs['RasterCalculator4']['OUTPUT']
        iface.addRasterLayer(outputs['RasterCalculator4']['OUTPUT'])
        return outputs, results

    def ejecutarFactorR(self, parameters, outputs, results):    # Ok
        # Multilevel b-spline interpolation
        alg_params = {
            'EPSILON': 0.0001,
            'FIELD': 'Precipitac',
            'LEVEL_MAX': 11,
            'METHOD': 0,
            'SHAPES': parameters['estaciones'],
            'TARGET_USER_FITS': 0,
            'TARGET_USER_SIZE': 35,
            'TARGET_USER_XMIN TARGET_USER_XMAX TARGET_USER_YMIN TARGET_USER_YMAX': parameters['DEM'],
            'TARGET_OUT_GRID': parameters['factor_R']
        }
        outputs['MultilevelBsplineInterpolation'] = processing.run('saga:multilevelbsplineinterpolation', alg_params)
        results['Factor_r'] = outputs['MultilevelBsplineInterpolation']['TARGET_OUT_GRID']
        iface.addRasterLayer(outputs['MultilevelBsplineInterpolation']['TARGET_OUT_GRID'])
        return outputs, results

    def ejecutarFactorK(self, parameters, outputs, results):    # Ok
        # v.surf.idw
        alg_params = {
            '-n': False,
            'GRASS_MIN_AREA_PARAMETER': 0.0001,
            'GRASS_RASTER_FORMAT_META': '',
            'GRASS_RASTER_FORMAT_OPT': '',
            'GRASS_REGION_CELLSIZE_PARAMETER': 0,
            'GRASS_REGION_PARAMETER': None,
            'GRASS_SNAP_TOLERANCE_PARAMETER': -1,
            'column': 'Factor_K',
            'input': parameters['edafologia'],
            'npoints': 12,
            'power': 2,
            'output': parameters['factor_K']
        }
        outputs['Vsurfidw'] = processing.run('grass7:v.surf.idw', alg_params)
        results['Factor_k'] = outputs['Vsurfidw']['output']
        iface.addRasterLayer(outputs['Vsurfidw']['output'])
        return outputs, results

    def ejecutarFactorC(self, parameters, outputs, results):
        # Rasterize (vector to raster)
        alg_params = {
            'BURN': 0,
            'DATA_TYPE': 5,
            'EXTENT': parameters['DEM'],
            'EXTRA': '',
            'FIELD': 'Factor_C',
            'HEIGHT': 39,
            'INIT': None,
            'INPUT': parameters['landuses'],
            'INVERT': False,
            'NODATA': 0,
            'OPTIONS': '',
            'UNITS': 0,
            'WIDTH': 39,
            'OUTPUT': parameters['factor_C']
        }
        outputs['RasterizeVectorToRaster'] = processing.run('gdal:rasterize', alg_params)
        results['Factor_c'] = outputs['RasterizeVectorToRaster']['OUTPUT']
        iface.addRasterLayer(outputs['RasterizeVectorToRaster']['OUTPUT'])
        return outputs, results

    def ejecutarFactorLS(self, parameters, outputs, results):

        # Raster calculator 5
        alg_params = {
            'CELLSIZE': 0,
            'CRS': None,
            'EXPRESSION': '\"factor_L@1\" * \"factor_S@1\"',
            'EXTENT': outputs['RasterCalculator3']['OUTPUT'],
            'LAYERS': [outputs['RasterCalculator3']['OUTPUT'], outputs['RasterCalculator4']['OUTPUT']],
            'OUTPUT': parameters['factor_L_S']
        }
        outputs['RasterCalculator5'] = processing.run('qgis:rastercalculator', alg_params)
        results['Factor_l_s'] = outputs['RasterCalculator5']['OUTPUT']

        iface.addRasterLayer(outputs['RasterCalculator5']['OUTPUT'])
        return outputs, results

    def ejecutarFactorA(self, parameters, outputs, results):
        # Raster calculator 6
        alg_params = {
            'CELLSIZE': 0,
            'CRS': None,
            'EXPRESSION': '\"factor_R@1\"  *  \"factor_K@1\" * \"factor_L_S@1\" * \"factor_C@1\"',
            'EXTENT': parameters['DEM'],
            'LAYERS': [outputs['MultilevelBsplineInterpolation']['TARGET_OUT_GRID'],outputs['Vsurfidw']['output'],outputs['RasterCalculator5']['OUTPUT'],outputs['RasterizeVectorToRaster']['OUTPUT']],
            'OUTPUT': parameters['factor_A']
        }
        outputs['RasterCalculator6'] = processing.run('qgis:rastercalculator', alg_params, )
        results['Factor_a'] = outputs['RasterCalculator6']['OUTPUT']
        iface.addRasterLayer(outputs['RasterCalculator6']['OUTPUT'])
        self.dlg.btn_ejecutar.clicked.disconnect(self.btnProcess_onClick)
        self.dlg.btn_ejecutar.setEnabled(True)
        return outputs, results


#  CON 500 LÍNEAS DE CÓDIGO, POR FIN TERMINÉ